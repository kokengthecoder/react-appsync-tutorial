import gql from 'graphql-tag'

export default gql`
subscription UpdatePostSub {
	updatedPost {
		__typename
		id
		author
		title
		version
	}
}`;