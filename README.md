## Description
This is a learning app that built with React, AWS AppSync, GraphqQL and DynamoDB by the tutorials as followed
- [DynamoDB Resolvers with AppSync GraphQL](https://docs.aws.amazon.com/appsync/latest/devguide/tutorial-dynamodb-resolvers.html)
- [Real-time Data](https://docs.aws.amazon.com/appsync/latest/devguide/real-time-data.html)
- [React with AppSync](https://docs.aws.amazon.com/appsync/latest/devguide/building-a-client-app-react.html)

## Demo Website
Deployed by Netlify
[https://react-appsync-tutorial.netlify.com/](https://react-appsync-tutorial.netlify.com/)

## Next Experiment
- Real time update with function updated and deleted